package com.telstra.codechallenge.useraccounts;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringBootUserAccountController {

  private SpringBootUserAccountService springBootuserAccountService;

  public SpringBootUserAccountController(
      SpringBootUserAccountService springBootuserAccountService) {
      this.springBootuserAccountService = springBootuserAccountService;
  }

  @RequestMapping(path = "/oldestUserAccount", method = RequestMethod.GET)
  public List<UserAccountModel> oldestUserAccount(@RequestParam Integer count) {
    return springBootuserAccountService.getOldestUserAccount(count);
  }

}
