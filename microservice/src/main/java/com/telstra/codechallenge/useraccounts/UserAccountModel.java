package com.telstra.codechallenge.useraccounts;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
public class UserAccountModel {
	
	@JsonProperty("id")
	private Integer id;
	
	@JsonProperty("login")
	private String login;
	
	@JsonProperty("html_url")
	private String htmlUrl;
}
