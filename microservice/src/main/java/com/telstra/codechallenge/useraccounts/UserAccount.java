package com.telstra.codechallenge.useraccounts;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UserAccount {

	@JsonProperty("total_count")
	private Integer totalCount;
	@JsonProperty("incomplete_results")
	private Boolean incompleteResults;
	@JsonProperty("items")
	private List<Item> items = null;

}