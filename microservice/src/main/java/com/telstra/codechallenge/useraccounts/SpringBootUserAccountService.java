package com.telstra.codechallenge.useraccounts;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SpringBootUserAccountService {

	@Value("${userAccount.base.url}")
	private String userAccountBaseUrl;

	private RestTemplate restTemplate;

	public SpringBootUserAccountService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	/**
	 * Returns a oldest user accounts with zero followers. Taken from
	 * https://spring.io/guides/gs/consuming-rest/.
	 *
	 * For requests using Basic Authentication, OAuth, or client ID and secret, you can make up to 30 requests per minute. 
     * For unauthenticated requests, the rate limit allows you to make up to 10 requests per minute.
	 * @return - a list of userAccountModel
	 */
	public List<UserAccountModel> getOldestUserAccount(Integer recordCount) {

		//If record count provide 0
		if (recordCount <= 0) {
			return new ArrayList<UserAccountModel>();
		}
		
		//the GitHub Search API provides up to 1,000 results for each search.
		if(recordCount > 1000) {
			recordCount = 1000;
		}

		List<Item> userList = new ArrayList<>();
		UserAccount userAccountData=null;

		// Setting page size as 100 as per default size of github
		int pageSize = 100;
		// If record count is less than default page size
		int numberOfPages = 1;

		int remainingRecord = recordCount % pageSize;

		if (recordCount > pageSize) {
			numberOfPages = recordCount / pageSize;
			if(remainingRecord > 0) {
				numberOfPages++;
			}
		}
		
		System.out.println("page size is" + numberOfPages);

		for (int page = 1; page <= numberOfPages; page++) {

			if (page == numberOfPages && remainingRecord > 0) {
				pageSize = remainingRecord;
			}
			
			try{
			 userAccountData = restTemplate.getForObject(userAccountBaseUrl
					+ "/users?q=followers:0&sort=joined&order=asc&page=" + page + "&per_page=" + pageSize,
					UserAccount.class);
			userList.addAll(userAccountData.getItems());

			}
			catch(Exception exception) {
				System.out.println("For unauthenticated requests, the rate limit allows you to make up to 10 requests per minute"+exception);
			}
			
		}
		
		return getOldestUserDetailModelList(userList);
	}

	private List<UserAccountModel> getOldestUserDetailModelList(List<Item> userList) {

		List<UserAccountModel> userAccountModelList = userList.stream().map((Item item) -> {
			return (new UserAccountModel(item.getId(), item.getLogin(), item.getHtmlUrl()));
		}).collect(Collectors.toList());

		return userAccountModelList;

	}
}
