# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
#For requests using Basic Authentication, OAuth, or client ID and secret, you can make up to 30 requests per minute. 
#For unauthenticated requests, the rate limit allows you to make up to 10 requests per minute.

Feature: As an api user I want to retrieve some spring boot oldest user account

  Scenario: Get oldest user account when count pass less than 1000
    * configure readTimeout = 10000
    Given url microserviceUrl
    And path '/oldestUserAccount'
    Given param count = 450
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def userSchema =  { id : '#number', login : '#string' , html_url : '#string'}
    # The response should have an array of 450 quote objects
    And match response == '#[450] userSchema' 

    Scenario: Get oldest user account when count pass as 0
    Given url microserviceUrl
    And path '/oldestUserAccount'
    Given param count = 0
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def userSchema =  { id : '#number', login : '#string' , html_url : '#string'}
    # The response should have an array of 0 quote objects
    And match response == '#[0] userSchema' 
    
    Scenario: Get oldest user account when count pass as negative
    Given url microserviceUrl
    And path '/oldestUserAccount'
    Given param count = 0
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def userSchema =  { id : '#number', login : '#string' , html_url : '#string'}
    # The response should have an array of 0 quote objects
    And match response == '#[0] userSchema' 
    
    Scenario: Get oldest user account when count is less than deafult page size
    Given url microserviceUrl
    And path '/oldestUserAccount'
    Given param count = 40
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://github.com/intuit/karate#schema-validation
    # Define the required schema
    * def userSchema =  { id : '#number', login : '#string' , html_url : '#string'}
    # The response should have an array of 40 quote objects
    And match response == '#[40] userSchema'
   
    